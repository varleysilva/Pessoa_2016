//diretivas de pré-compilador
#ifndef PESSOA_HPP  //se nao estiver definido no compilador (if not defined), bom para evitar erros de compilacao quando tem muitos arquivos e muitos includes
#define PESSOA_HPP
//

#include <iostream>
#include <string>

using namespace std;

class Pessoa {
  
  //atributos
  private:
    string nome;
    string telefone;
    int idade;

  //metodos
  public:
    Pessoa(); //construtor
    ~Pessoa();  //destrutor
    void setNome(string nome); //acessor
    string getNome();
    void setTelefone(string telefone);
    string getTelefone();
    void setIdade(int idade);
    int getIdade();
    void incrementaIdade();

};

#endif

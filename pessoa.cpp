#include "pessoa.hpp"
#include <string>

using namespace std;

Pessoa::Pessoa() {
  nome = "Fulano";
  idade = 0;
  telefone = "";
}

Pessoa::~Pessoa() {
  
}

void Pessoa::setNome(string nome) {
  this->nome = nome; //(diretiva this, 'this class)
}
string Pessoa::getNome() {
  return nome;
}

void Pessoa::setTelefone(string telefone) {
  this->telefone = telefone;
}
string Pessoa::getTelefone() {
  return telefone;
}

void Pessoa::setIdade(int idade) {
  this->idade = idade;
}

int Pessoa::getIdade() {
  return idade;
}

void Pessoa::incrementaIdade() {
  idade = idade + 1;
}


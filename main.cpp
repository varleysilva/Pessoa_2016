#include <iostream>
#include "pessoa.hpp"
using namespace std;

int main (int argc, char ** argv) {

  Pessoa pessoa_1;
  Pessoa pessoa_2;
  Pessoa * pessoa_3;

  pessoa_3 = new Pessoa();

  pessoa_3->setNome("Pedro");
  pessoa_3->setTelefone("4992-8922");
  pessoa_3->setIdade(21);

  pessoa_1.setNome("Joao");
  pessoa_1.setTelefone("6666-6666");
  pessoa_1.setIdade(23);

  pessoa_2.setNome("Maria");
  pessoa_2.setTelefone("3333-6666");
  pessoa_2.setIdade(19);

  //string nome_da_pessoa;

  //nome_da_pessoa = pessoa_1.getNome();

  cout << endl << "Pessoa 1" << endl;
  cout << "Nome: " << pessoa_1.getNome() << endl;
  cout << "Telefone: " << pessoa_1.getTelefone() << endl;
  cout << "Idade: " << pessoa_1.getIdade() << endl;

  cout << endl << "Pessoa 2" << endl;
  cout << "Nome: " << pessoa_2.getNome() << endl;
  cout << "Telefone: " << pessoa_2.getTelefone() << endl;
  cout << "Idade: " << pessoa_2.getIdade() << endl;

  cout << endl << "Pessoa 3" << endl;
  cout << "Nome: " << pessoa_3->getNome() << endl;
  cout << "Telefone: " << pessoa_3->getTelefone() << endl;
  cout << "Idade: " << pessoa_3->getIdade() << endl;

  delete(pessoa_3);


}
